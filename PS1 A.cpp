﻿#include <iostream>
using namespace std;

int main()
{
    int wybor;
    int i;
    cout << "Wybierz co chcesz zrobic" << endl;
    cout << "1. Wypisanie liczb od 1 do 10" << endl;
    cout << "2. Wypisanie liczb od 10 do 1" << endl;
    cin >> wybor;
    cout << "Wybrales: " << wybor << endl;
    if (wybor == 1)
    {
        for (i = 0; i <= 10; i++)
            cout << i << endl;
    }
    else
    {
        for (i = 10; i >= 0; i--)
            cout << i << endl;
    }
    return 0;
}